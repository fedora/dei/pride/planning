# Pride - Issues and Discussions
Welcome to Fedora Pride, a subgroup within the Fedora DEI community dedicated to supporting the needs of LGBTQ+ community members, contributors, and users. Our mission is to focus on the needs of LGBTQ+ community and cultivate an environment where everyone feels welcome, empowered, and encouraged to participate actively in our community. 

## What's Happening Here?
The Fedora Pride team utilizes this repository to document our ongoing efforts and to address issues that require targeted solutions. Our team gathers weekly to discuss these matters, ensuring that our community remains an inclusive space for all. To learn more about the Fedora Pride team, visit our documentation page.

## Submit a New Request
To submit a new request or concern, please open a new issue.

## How to Connect with the Fedora Pride Team
In addition to this repository, you can find members of the Fedora Pride team in various online spaces:

- Discourse Forum
- Element/Matrix: #fedora-pride


Join us as we champion LGBTQ+ contributors and users in the Fedora community.